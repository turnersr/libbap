// libbap.c
// Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

#include <python2.7/Python.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/custom.h>

// Copied from https://github.com/ocamllabs/ocaml-ctypes/commit/6ede17ae0749d4660bbb27719ad2c523e616ae24#src/unsigned_stubs.h
#include "ctypes_unsigned_stubs.h"

typedef struct {
  PyObject_HEAD

  value term;
} BAP;

typedef struct {
  PyObject_HEAD

  value caml_self;
  PyObject* obj;
} VolatilityObject;

static PyObject* vol_none_object;

PyObject* ocaml_to_python(value caml_term) {
  PyObject* py_term;
  PyObject* dict_key;
  PyObject* dict_val;
  int i;

  CAMLparam1(caml_term);
  CAMLlocal2(caml_tuple, list_cons);

  if (Is_long(caml_term)) {
    // Constant constructors
    switch(Int_val(caml_term)) {
      case 0:
        // PyNone
        py_term = Py_None;
        break;
      default:
        PyErr_SetString(PyExc_TypeError, "ocaml_to_python: constant constructor term has an unrecognised type");
        py_term = NULL;
    }
  } else {
    // Non constant constructors
    switch(Tag_val(caml_term)) {
      case 0: 
        // PyInt of UInt64.t
        py_term = PyLong_FromUnsignedLongLong(Uint64_val(Field(caml_term, 0)));
        break;
      case 1:
        // PyBool of bool
        py_term = PyBool_FromLong(Bool_val(Field(caml_term, 0)));
        break;
      case 2:
        // PyFloat of double
        py_term = PyFloat_FromDouble(Double_val(Field(caml_term, 0)));
        break;
      case 3:
        // PyString of string
        py_term = PyString_FromStringAndSize(String_val(Field(caml_term, 0)), caml_string_length(Field(caml_term, 0)));
        break;
      case 4:
        // PyTuple of py_object array
        caml_tuple = Field(caml_term, 0);
        py_term = PyTuple_New(Wosize_val(caml_tuple));
        for(i = 0; i < Wosize_val(caml_tuple); i++) {
          if (PyTuple_SetItem(py_term, i, ocaml_to_python(Field(caml_tuple, i))) != 0) {
            // error occurred - assuming PyErr is already set
            Py_CLEAR(py_term);
            py_term = NULL;
            break;
          }
        }
        break;
      case 5:
        // PyList of py_object list
        list_cons = Field(caml_term, 0);
        py_term = PyList_New(0);
        while(!Is_long(list_cons)) {
          if (PyList_Append(py_term, ocaml_to_python(Field(list_cons, 0))) == 0) {
            list_cons = Field(list_cons, 1);
          } else {
            // error occurred - assuming PyErr is already set
            Py_CLEAR(py_term);
            py_term = NULL;
            break;
          }
        }
        break;
      case 6:
        // PyDict of (py_object * py_object) list
        list_cons = Field(caml_term, 0);
        py_term = PyDict_New();
        while(!Is_long(list_cons)) {
          dict_key = ocaml_to_python(Field(Field(list_cons, 0), 0));
          dict_val = ocaml_to_python(Field(Field(list_cons, 0), 1));
          if (!PyErr_Occurred() && PyDict_SetItem(py_term, dict_key, dict_val) == 0) {
            list_cons = Field(list_cons, 1);
          } else {
            // error occurred - assuming PyErr is already set
            Py_CLEAR(py_term);
            py_term = NULL;
            break;
          }
        }
        break;
      case 7:
        // PyObject of vol_obj (vol_obj <: VolatilityObject)
        py_term = (PyObject*)Int64_val(Field(caml_term, 0)); // FIXME:
        break;
      default:
        PyErr_SetString(PyExc_TypeError, "ocaml_to_python: non-constant constructor term has an unrecognised type");
        py_term = NULL;
    }
  }
  CAMLreturnT(PyObject*, py_term);
}

staticforward PyTypeObject VolatilityObjectType;
staticforward PyObject* VolatilityObject_new(PyTypeObject *type, PyObject *args, PyObject *kwargs);

value python_to_ocaml(PyObject* py_term) {
  PyObject* dict_keys;
  PyObject* ptype;
  PyObject* pvalue;
  PyObject* ptraceback;
  int i;

  CAMLparam0();
  CAMLlocal5(caml_term, list_cons, dict_entry, tuple_term, item);

  if (py_term == Py_None) {
    // PyNone
    caml_term = Val_long(0);
  } else if (PyObject_IsInstance(py_term, vol_none_object)) {
      // PyNone
      caml_term = Val_long(0);
  } else if (PyInt_Check(py_term)) {
      // PyInt of UInt64.t
      caml_term = caml_alloc(1, 0);
      item = ctypes_copy_uint64(PyInt_AsUnsignedLongLongMask(py_term));
      Field(caml_term, 0) = item;
  } else if (PyLong_Check(py_term)) {
      // PyInt of UInt64.t
      caml_term = caml_alloc(1, 0);
      item = ctypes_copy_uint64(PyLong_AsUnsignedLongLong(py_term));
      Field(caml_term, 0) = item;
  } else if (PyBool_Check(py_term)) {
      // PyBool of bool
      caml_term = caml_alloc(1, 1);
      Field(caml_term, 0) = Val_bool(py_term == Py_True);
  } else if (PyFloat_Check(py_term)) {
      // PyFloat of float
      caml_term = caml_alloc(1, 2);
      Field(caml_term, 0) = copy_double(PyFloat_AsDouble(py_term));
  } else if (PyString_Check(py_term)) {
      // PyString of string
      caml_term = caml_alloc(1, 3);
      item = alloc_string(PyString_Size(py_term));
      Field(caml_term, 0) = item;
      for(i = 0; i < PyString_Size(py_term); i++) {
        Byte_u(item, i) = PyString_AsString(py_term)[i];
      }
  } else if (PyTuple_Check(py_term)) {
      // PyTuple of py_object array
      caml_term = caml_alloc(1, 4);
      tuple_term = alloc_tuple(PyTuple_Size(py_term));
      Field(caml_term, 0) = tuple_term;
      for(i = 0; i < PyTuple_Size(py_term); i++) {
        item = python_to_ocaml(PyTuple_GetItem(py_term, i));
        Field(tuple_term, i) = item;
      }
  } else if (PyList_Check(py_term)) {
      // PyList of py_object list
      caml_term = caml_alloc(1, 5);
      Field(caml_term, 0) = Val_emptylist;
      for(i = PyList_Size(py_term) - 1; i >= 0; i--) {
        list_cons = alloc_tuple(2);
        item = python_to_ocaml(PyList_GetItem(py_term, i));
        Field(list_cons, 0) = item;
        Field(list_cons, 1) = Field(caml_term, 0);
        Field(caml_term, 0) = list_cons;
      }
  } else if (PyDict_Check(py_term)) {
      // PyDict of (py_object * py_object) list
      caml_term = caml_alloc(1, 6);
      Field(caml_term, 0) = Val_emptylist;
      dict_keys = PyDict_Keys(py_term);
      for(i = 0; i < PyList_Size(dict_keys); i++) {
        list_cons = alloc_tuple(2);
        dict_entry = alloc_tuple(2);
        item = python_to_ocaml(PyList_GetItem(dict_keys, i));
        Field(dict_entry, 0) = item;
        item = python_to_ocaml(PyDict_GetItem(py_term, PyList_GetItem(dict_keys, i)));
        Field(dict_entry, 1) = item;
        Field(list_cons, 0) = dict_entry;
        Field(list_cons, 1) = Field(caml_term, 0);
        Field(caml_term, 0) = list_cons;
      }
      Py_XDECREF(dict_keys);
  } else {
      // PyObject of vol_obj
      caml_term = caml_alloc(1, 7);
      if (PyObject_IsInstance(py_term, (PyObject*)(&VolatilityObjectType))) {
        // py_term is already an instance of VolatilityObject
        Field(caml_term, 0) = ((VolatilityObject*)py_term)->caml_self;
        Py_XINCREF(py_term);
      } else {
        // py_term is not an instance of VolatilityObjectType
        item = ((VolatilityObject*)(VolatilityObject_new(&VolatilityObjectType, Py_BuildValue("(O)", py_term), NULL)))->caml_self;
        Field(caml_term, 0) = item;
      }
  }

  // Check that no Python exceptions have occurred during previous checks etc.
  if (PyErr_Occurred()) {
    PyErr_Fetch(&ptype, &pvalue, &ptraceback);
    PyErr_Clear();
    Py_XDECREF(py_term);
    if (pvalue != NULL) {
      raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(pvalue));
    } else {
      raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(PyObject_Str(ptype)));
    }
    Py_XDECREF(ptype);
    Py_XDECREF(pvalue);
    Py_XDECREF(ptraceback);
  }

  CAMLreturn(caml_term);
}

void volatility_finalize(value caml_self) {
  VolatilityObject *self;

  self = (VolatilityObject*)Data_custom_val(caml_self);

  self->caml_self = NULL;
  Py_XDECREF(self);
}

static struct custom_operations volatility_self_ops = {
  "volatility.ocaml.self",
  &volatility_finalize,
  custom_compare_default,
  custom_hash_default,
  custom_serialize_default,
  custom_deserialize_default
};

static PyObject* VolatilityObject_new(PyTypeObject *type, PyObject *args, PyObject *kwargs) {
  VolatilityObject *self;

  self = (VolatilityObject*)PyType_GenericNew(type, args, kwargs);
  if (self == NULL) {
    return NULL;
  }

  if (!PyArg_ParseTuple(args, "O", &(self->obj))) {
    PyErr_SetString(PyExc_TypeError, "Need to supply a Python object instance (of volatility.obj.BaseObject)");

    return NULL;
  }

  register_global_root(&(self->caml_self));
  self->caml_self = alloc_custom(&volatility_self_ops, sizeof(VolatilityObject*), 0, 1);
  Field(self->caml_self, 1) = self;

  Py_XINCREF(self);
  Py_XINCREF(self->obj);

  return (PyObject*)self;
}

static void VolatilityObject_dealloc(VolatilityObject *self) {
  Py_XDECREF(self->obj);
  remove_global_root(&(self->caml_self));
  self->ob_type->tp_free((PyObject*)self);
}

value VolatilityObject_callback(value caml_self, value py_eval_list) {
  VolatilityObject *self;
  PyObject* ptype;
  PyObject* pvalue;
  PyObject* ptraceback;
  PyObject* method;
  PyObject* result;
  char* name;
  PyObject* args;
  PyObject* kwargs;

  CAMLparam2(caml_self, py_eval_list);
  CAMLlocal3(list_cons, data, caml_result);

  self = (VolatilityObject*)Field(caml_self, 1);

  result = self->obj;
  list_cons = py_eval_list;
  while (!Is_long(list_cons)) {
    data = Field(list_cons, 0);
    switch(Tag_val(data)) {
      case 0:
        // PyAttribute of string
        name = String_val(Field(data, 0));
        result = PyObject_GetAttrString(result, name);
        if (result == NULL) {
          if (PyErr_Occurred()) {
            PyErr_Fetch(&ptype, &pvalue, &ptraceback);
            PyErr_Clear();
            if (pvalue != NULL) {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(pvalue));
            } else {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(PyObject_Str(ptype)));
            }
            Py_XDECREF(ptype);
            Py_XDECREF(pvalue);
            Py_XDECREF(ptraceback);
          } else {
            raise_with_string(*caml_named_value("VolatilityException"), "VolatilityObject_callback: failed to evaluate a PyAttribute instance");
          }
        }
        break;
      case 1:
        // PyMethod of string * py_object * py_object
        name = String_val(Field(data, 0));
        args = ocaml_to_python(Field(data, 1));
        kwargs = ocaml_to_python(Field(data, 2));
        method = PyObject_GetAttrString(result, name);
        if (method != NULL) {
          if (PyDict_Size(kwargs) == 0) {
            result = PyObject_Call(method, args, NULL);
          } else {
            result = PyObject_Call(method, args, kwargs);
          }
        }
        Py_XDECREF(args);
        Py_XDECREF(kwargs);
        Py_XDECREF(method);
        if (method == NULL || result == NULL) {
          if (PyErr_Occurred()) {
            PyErr_Fetch(&ptype, &pvalue, &ptraceback);
            PyErr_Clear();
            if (pvalue != NULL) {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(pvalue));
            } else {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(PyObject_Str(ptype)));
            }
            Py_XDECREF(ptype);
            Py_XDECREF(pvalue);
            Py_XDECREF(ptraceback);
          } else {
            raise_with_string(*caml_named_value("VolatilityException"), "VolatilityObject_callback: failed to evaluate a PyMethod instance");
          }
        }
        break;
      case 2:
        // PyCall of py_object * py_object
        args = ocaml_to_python(Field(data, 0));
        kwargs = ocaml_to_python(Field(data, 1));
        if (PyObject_Size(kwargs) == 0) {
          result = PyObject_Call(result, args, NULL);
        } else {
          result = PyObject_Call(result, args, kwargs);
        }
        Py_XDECREF(args);
        Py_XDECREF(kwargs);
        if (result == NULL) {
          if (PyErr_Occurred()) {
            PyErr_Fetch(&ptype, &pvalue, &ptraceback);
            PyErr_Clear();
            if (pvalue != NULL) {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(pvalue));
            } else {
              raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(PyObject_Str(ptype)));
            }
            Py_XDECREF(ptype);
            Py_XDECREF(pvalue);
            Py_XDECREF(ptraceback);
          } else {
            raise_with_string(*caml_named_value("VolatilityException"), "VolatilityObject_callback: failed to evaluate a PyCall instance");
          }
        }
        break;
      default:
        if (PyErr_Occurred()) {
          PyErr_Fetch(&ptype, &pvalue, &ptraceback);
          PyErr_Clear();
          if (pvalue != NULL) {
            raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(pvalue));
          } else {
            raise_with_string(*caml_named_value("VolatilityException"), PyString_AsString(PyObject_Str(ptype)));
          }
          Py_XDECREF(ptype);
          Py_XDECREF(pvalue);
          Py_XDECREF(ptraceback);
        } else {
          raise_with_string(*caml_named_value("VolatilityException"), "VolatilityObject_callback: invalid tag type for py_eval");
        }
    }
    list_cons = Field(list_cons, 1);
  }

  caml_result = python_to_ocaml(result);
  Py_XDECREF(result);
  CAMLreturn(caml_result);
}

static PyObject* Program_new(PyTypeObject *type, PyObject *args, PyObject *kwargs) {
  BAP *self;
  char* arch;
  char* bytes;
  int bytes_len;
  unsigned long long addr;

  if (!PyArg_ParseTuple(args, "ss#L", &arch, &bytes, &bytes_len, &addr)) {
    PyErr_SetString(PyExc_TypeError, "Usage: Program(<arch>: string, <raw data>: string, <addr>: int64)");

    return NULL;
  }

  if (bytes_len == 0) {
    PyErr_SetString(PyExc_ValueError, "Raw binary string is empty");

    return NULL;
  }

  self = (BAP*)PyType_GenericNew(type, args, kwargs);
  if (self == NULL) {
    return NULL;
  }

  value v_arch, v_bytes, v_addr;

  CAMLparam3(v_arch, v_bytes, v_addr);
  CAMLlocal2(result, exn);

  v_arch = copy_string(arch);
  v_bytes = alloc_string(bytes_len);
  memcpy(String_val(v_bytes), bytes, bytes_len);
  v_addr = copy_int64(addr);

  result = caml_callback3_exn(*caml_named_value("byte_sequence_to_bap"), v_arch, v_bytes, v_addr);
  if (Is_exception_result(result)) {
    exn = Extract_exception(result);
    
    if (Wosize_val(exn) == 2) {
      PyErr_SetString(PyExc_ValueError, String_val(Field(exn, 1)));
    } else {
      PyErr_SetString(PyExc_ValueError, "__new__: unknown OCaml exception caught");
    }
    CAMLreturnT(PyObject*, NULL);
  }

  register_global_root(&(self->term));
  self->term = result;

  CAMLreturnT(PyObject*, (PyObject*)self);
}

static void Program_dealloc(BAP *self) {
  remove_global_root(&(self->term));
  self->ob_type->tp_free((PyObject*)self);
}

static PyObject* BAP_str(BAP *self) {
  if (self->term == NULL) {
    PyErr_SetString(PyExc_TypeError, "__str__: need to __init__ object (currently we encapsulate a null BAP term)");

    return NULL;
  }

  CAMLparam0();
  CAMLlocal2(result, exn);

  result = caml_callback_exn(*caml_named_value("bap_to_string"), self->term);
  if (Is_exception_result(result)) {
    exn = Extract_exception(result);
    
    if (Wosize_val(exn) == 2) {
      PyErr_SetString(PyExc_RuntimeError, String_val(Field(exn, 1)));
    } else {
      PyErr_SetString(PyExc_RuntimeError, "__str__: unknown OCaml exception caught");
    }
    CAMLreturnT(PyObject*, NULL);
  }
  
  CAMLreturnT(PyObject*, Py_BuildValue("s", String_val(result)));
}

static PyObject* BAP_call(BAP* self, PyObject* args, PyObject* kwargs) {
  PyObject* cmds;
  Py_ssize_t num_cmds, cmd_size, i, j;
  PyObject* cmd;
  PyObject* cmd_str;

  if (self->term == NULL) {
    PyErr_SetString(PyExc_TypeError, "__call__: need to __init__ object (currently we encapsulate a null BAP term)");

    return NULL;
  }

  if (!PyArg_ParseTuple(args, "O", &cmds)) {
    PyErr_SetString(PyExc_TypeError, "__call__: expected to be passed a PyObject argument");

    return NULL;
  }

  if (!PyList_Check(cmds)) {
    PyErr_SetString(PyExc_TypeError, "__call__: expected a list of commands");

    return NULL;
  }

  num_cmds = PyList_Size(cmds);
  if (num_cmds <= 0) {
    // No work to do, so simply return
    return Py_None;
  }

  value ocaml_cmds;

  CAMLparam1(ocaml_cmds);
  CAMLlocal5(ocaml_cmd, cons_cmds, item, result, exn);

  ocaml_cmds = Val_emptylist;
  for (i=num_cmds-1; i >= 0; i--) {
    cmd = PyList_GetItem(cmds, i);
    if (cmd == NULL || !PyList_Check(cmd)) {
      PyErr_SetString(PyExc_TypeError, "__call__: expected each command to be a list");

      CAMLreturnT(PyObject*, NULL);
    }

    cmd_size = PyList_Size(cmd);
    if (cmd_size <= 1) {
      PyErr_SetString(PyExc_TypeError, "__call__: expected each command list to contain a type and a command");

      CAMLreturnT(PyObject*, NULL);
    }

    ocaml_cmd = alloc_tuple(cmd_size);
    for(j=0; j < cmd_size; j++) {
      cmd_str = PyList_GetItem(cmd, j);
      if (cmd_str == NULL || !PyString_Check(cmd_str)) {
        PyErr_SetString(PyExc_TypeError, "__call__: expected each command list to be a list of strings");

        CAMLreturnT(PyObject*, NULL);
      }

      item = copy_string(PyString_AsString(cmd_str));
      Store_field(ocaml_cmd, j, item);
    }

    cons_cmds = alloc_tuple(2);
    Field(cons_cmds, 0) = ocaml_cmd;
    Field(cons_cmds, 1) = ocaml_cmds;
    ocaml_cmds = cons_cmds;
  }

  result = caml_callback2_exn(*caml_named_value("apply_cmd"), ocaml_cmds, self->term);
  if (Is_exception_result(result)) {
    exn = Extract_exception(result);

    if (Wosize_val(exn) == 2) {
      PyErr_SetString(PyExc_RuntimeError, String_val(Field(exn, 1)));
    } else {
      PyErr_SetString(PyExc_RuntimeError, "__call__: unknown OCaml exception caught");
    }

    CAMLreturnT(PyObject*, NULL);
  }

  CAMLreturnT(PyObject*, ocaml_to_python(result));
}

static PyObject* VolatilityObject_call(VolatilityObject* self, PyObject* args, PyObject* kwargs) {
  PyObject* cmd;

  if (!PyArg_ParseTuple(args, "s", &cmd)) {
    PyErr_SetString(PyExc_TypeError, "__call__: expected to be passed an OCaml function (string) name");

    return NULL;
  }

  CAMLparam0();
  CAMLlocal2(result, exn);

  result = caml_callback_exn(*caml_named_value(cmd), self->caml_self);
  if (Is_exception_result(result)) {
    exn = Extract_exception(result);

    if (Wosize_val(exn) == 2) {
      PyErr_SetString(PyExc_RuntimeError, String_val(Field(exn, 1)));
    } else {
      PyErr_SetString(PyExc_RuntimeError, "__call__: unknown OCaml exception caught");
    }

    CAMLreturnT(PyObject*, NULL);
  }

  CAMLreturnT(PyObject*, ocaml_to_python(result));
}

static PyTypeObject VolatilityObjectType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /* ob_size */
  "VolatilityObject",        /* tp_name */
  sizeof(VolatilityObject),  /* tp_basicsize */
  0,                         /* tp_itemsize */
  (destructor)VolatilityObject_dealloc, /* tp_dealloc */
  0,                         /* tp_print */
  0,                         /* tp_getattr */
  0,                         /* tp_setattr */
  0,                         /* tp_compare */
  0,                         /* tp_repr */
  0,                         /* tp_as_number */
  0,                         /* tp_as_sequence */
  0,                         /* tp_as_mapping */
  0,                         /* tp_hash */
  (ternaryfunc)VolatilityObject_call, /* tp_call */
  0,                         /* tp_str */
  PyObject_GenericGetAttr,   /* tp_getattro */
  0,                         /* tp_setattro */
  0,                         /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags*/
  "OCaml/Volatility interface - allows volatility.obj.BaseObject instances to have methods called from OCaml", /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  0,                         /* tp_methods */
  0,                         /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  0,                         /* tp_init */
  0,                         /* tp_alloc */
  VolatilityObject_new,      /* tp_new */
};

static PyTypeObject ProgramType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /* ob_size */
  "Program",                 /* tp_name */
  sizeof(BAP),               /* tp_basicsize */
  0,                         /* tp_itemsize */
  (destructor)Program_dealloc, /* tp_dealloc */
  0,                         /* tp_print */
  0,                         /* tp_getattr */
  0,                         /* tp_setattr */
  0,                         /* tp_compare */
  0,                         /* tp_repr */
  0,                         /* tp_as_number */
  0,                         /* tp_as_sequence */
  0,                         /* tp_as_mapping */
  0,                         /* tp_hash */
  (ternaryfunc)BAP_call,     /* tp_call */
  (reprfunc)BAP_str,         /* tp_str */
  PyObject_GenericGetAttr,   /* tp_getattro */
  0,                         /* tp_setattro */
  0,                         /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags*/
  "Converts all instructions (in a byte string) into a BAP IL term - uses linear disassembly", /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  0,                         /* tp_methods */
  0,                         /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  0,                         /* tp_init */
  0,                         /* tp_alloc */
  Program_new,               /* tp_new */
};

void initlibbap(void) {
  PyObject* mod;
  PyObject* obj_mod;
  char *argv[2];
  
  argv[0] = "python";
  argv[1] = NULL;
  
  // Start up the OCaml environment
  caml_startup(argv);

  // Create the module
  mod = Py_InitModule3("libbap", NULL, "Python/Volatility interface to the Binary Analysis Platform (BAP)");
  if (mod == NULL) {
    return;
  }

  // Fill in some slots in the types, and make them ready
  if (PyType_Ready(&VolatilityObjectType) < 0) {
    return;
  }
  if (PyType_Ready(&ProgramType) < 0) {
    return;
  }

  // Add the types to the module.
  Py_INCREF(&VolatilityObjectType);
  PyModule_AddObject(mod, "VolatilityObject", (PyObject*)&VolatilityObjectType);
  Py_INCREF(&ProgramType);
  PyModule_AddObject(mod, "Program", (PyObject*)&ProgramType);

  obj_mod = PyImport_ImportModule("volatility.obj");
  if (obj_mod == NULL) {
    PyErr_SetString(PyExc_ValueError, "Failed to import volatility.obj");
    return;
  }
  vol_none_object = PyDict_GetItemString(PyModule_GetDict(obj_mod), "NoneObject");
}
