(* bap_ast.ml                                                              *)
(* Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>                   *)
(*                                                                         *)
(* This program is free software; you can redistribute it and/or modify    *)
(* it under the terms of the GNU General Public License as published by    *)
(* the Free Software Foundation; either version 2 of the License, or (at   *)
(* your option) any later version.                                         *)
(*                                                                         *)
(* This program is distributed in the hope that it will be useful, but     *)
(* WITHOUT ANY WARRANTY; without even the implied warranty of              *)
(* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU        *)
(* General Public License for more details.                                *)
(*                                                                         *)
(* You should have received a copy of the GNU General Public License       *)
(* along with this program; if not, write to the Free Software             *)
(* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA *)

open Volatility
open Type
open Ast

let int_to_python n = PyInt(Unsigned.UInt64.of_int n)

let int64_to_python n = PyInt(Unsigned.UInt64.of_int(Int64.to_int n))

let rec typ_to_python = function
	| Reg(n) ->
			PyDict [ (PyString "reg", int_to_python n) ]
  | TMem(t) ->
  		PyDict [ (PyString "tmem", typ_to_python t) ]
  | Array(t1, t2) ->
  		PyDict [ (PyString "array", PyTuple [| typ_to_python t1; typ_to_python t2 |]) ]

let var_to_python var =
	PyDict [ (PyString "v", PyTuple [| int_to_python(Var.hash var); PyString(Var.name var); typ_to_python(Var.typ var) |]) ]

let vars_to_python var_list = PyList(List.map var_to_python var_list)

let label_to_python = function
  | Name(s) ->
  		PyDict [ (PyString "name", PyString s) ]
  | Addr(a) ->
  		PyDict [ (PyString "addr", int64_to_python a) ]

let usage_to_python = function
	| RD ->
			PyString "rd"
	| WR -> 
			PyString "wr"
	| RW ->
			PyString "rw"

let taint_to_python = function
	| Taint(i) ->
			PyDict [ (PyString "taint", int_to_python i) ]

let big_int_to_python big_int = PyString(Big_int_Z.string_of_big_int big_int)

let exn_to_python ex = PyString(Printexc.to_string ex)

let context_to_python context =
	PyDict [ (PyString "name", PyString(context.name)); (PyString "mem", PyBool(context.mem)); (PyString "t", typ_to_python(context.t)); (PyString "index", int64_to_python(context.index)); (PyString "value", big_int_to_python(context.value)); (PyString "usage", usage_to_python(context.usage)); (PyString "taint", taint_to_python(context.taint)) ]

let attr_to_python = function
  | Pos(s, i) ->
  		PyDict [ (PyString "pos", PyTuple [| PyString s; int_to_python i |]) ]
  | Asm(s) ->
  		PyDict [ (PyString "asm", PyString s) ]
  | Address(a) ->
  		PyDict [ (PyString "address", int64_to_python a) ]
  | Liveout ->
  		PyString "liveout"
  | StrAttr(s) ->
  		PyDict [ (PyString "strattr", PyString s) ]
  | Context(c) ->
  		PyDict [ (PyString "context", context_to_python c) ]
  | ThreadId(i) ->
  		PyDict [ (PyString "threadid", int_to_python i) ]
  | ExnAttr(e) ->
  		PyDict [ (PyString "exnattr", exn_to_python e) ]
  | InitRO ->
  		PyString "initro"
  | Synthetic ->
  		PyString "synthetic"

let attrs_to_python attr_list = PyList(List.map attr_to_python attr_list)

let cast_to_python = function
  | CAST_UNSIGNED ->
  		PyString "cast_unsigned"
  | CAST_SIGNED ->
  		PyString "cast_signed"
  | CAST_HIGH ->
  		PyString "cast_high"
  | CAST_LOW ->
  		PyString "cast_low"

let binop_to_python = function
  | PLUS ->
  		PyString "plus"
  | MINUS ->
  		PyString "minus"
  | TIMES ->
  		PyString "times"
  | DIVIDE ->
  		PyString "divide"
  | SDIVIDE ->
  		PyString "sdivide"
  | MOD ->
  		PyString "mod"
  | SMOD ->
  		PyString "smod"
  | LSHIFT ->
  		PyString "lshift"
  | RSHIFT ->
  		PyString "rshift"
  | ARSHIFT ->
  		PyString "arshift"
  | AND ->
  		PyString "and"
  | OR ->
  		PyString "or"
  | XOR ->
  		PyString "xor"
  | EQ ->
  		PyString "eq"
  | NEQ ->
  		PyString "neq"
  | LT ->
  		PyString "lt"
  | LE ->
  		PyString "le"
  | SLT ->
  		PyString "slt"
  | SLE ->
  		PyString "sle"

let unop_to_python = function
  | NEG ->
  		PyString "neg"
  | NOT ->
  		PyString "not"

let rec exp_to_python = function
  | Load(e1, e2, e3, t) ->
  		PyDict [ (PyString "load", PyTuple [| exp_to_python e1; exp_to_python e2; exp_to_python e3; typ_to_python t |]) ]
  | Store(e1, e2, e3, e4, t) ->
  		PyDict [ (PyString "store", PyTuple [| exp_to_python e1; exp_to_python e2; exp_to_python e3; exp_to_python e4; typ_to_python t |]) ]
  | BinOp(op, e1, e2) ->
  		PyDict [ (PyString "binop", PyTuple [| binop_to_python op; exp_to_python e1; exp_to_python e2 |]) ]
  | UnOp(op, e) ->
  		PyDict [ (PyString "unop", PyTuple [| unop_to_python op; exp_to_python e |]) ]
  | Var(v) ->
  		PyDict [ (PyString "var", var_to_python v) ]
  | Lab(s) ->
  		PyDict [ (PyString "lab", PyString s) ]
  | Int(i, t) ->
  		PyDict [ (PyString "int", PyTuple [| big_int_to_python i; typ_to_python t |]) ]
  | Cast(ct, t, e) ->
  		PyDict [ (PyString "cast", PyTuple [| cast_to_python ct; typ_to_python t; exp_to_python e |]) ]
  | Let(v, e1, e2) ->
  		PyDict [ (PyString "let", PyTuple [| var_to_python v; exp_to_python e1; exp_to_python e2 |]) ]
  | Unknown(s, t) ->
  		PyDict [ (PyString "unknown", PyTuple [| PyString s; typ_to_python t |]) ]
  | Ite(eB, eT, eF) ->
  		PyDict [ (PyString "ite", PyTuple [| exp_to_python eB; exp_to_python eT; exp_to_python eF |]) ]
  | Extract(i1, i2, e) ->
  		PyDict [ (PyString "extract", PyTuple [| big_int_to_python i1; big_int_to_python i2; exp_to_python e |]) ]
  | Concat(e1, e2) ->
  		PyDict [ (PyString "concat", PyTuple [| exp_to_python e1; exp_to_python e2 |]) ]

let stmt_to_python = function
  | Move(v, e, a) ->
  		PyDict [ (PyString "move", PyTuple [| var_to_python v; exp_to_python e; attrs_to_python a |]) ]
  | Jmp(e, a) ->
  		PyDict [ (PyString "jmp", PyTuple [| exp_to_python e; attrs_to_python a |]) ]
  | CJmp(eB, eT, eF, a) ->
  		PyDict [ (PyString "cjmp", PyTuple [| exp_to_python eB; exp_to_python eT; exp_to_python eF; attrs_to_python a |]) ]
  | Label(l, a) ->
  		PyDict [ (PyString "label", PyTuple [| label_to_python l; attrs_to_python a |]) ]
  | Halt(e, a) ->
  		PyDict [ (PyString "halt", PyTuple [| exp_to_python e; attrs_to_python a |]) ]
  | Assert(e, a) ->
  		PyDict [ (PyString "assert", PyTuple [| exp_to_python e; attrs_to_python a |]) ]
  | Assume(e, a) ->
  		PyDict [ (PyString "assume", PyTuple [| exp_to_python e; attrs_to_python a |]) ]
  | Comment(s, a) ->
  		PyDict [ (PyString "comment", PyTuple [| PyString s; attrs_to_python a |]) ]
  | Special(s, a) ->
  		PyDict [ (PyString "special", PyTuple [| PyString s; attrs_to_python a |]) ]

let to_python stmt_list = PyList(List.map stmt_to_python stmt_list)
