#!/usr/bin/env python
#
# __init__.py
# Copyright (C) 2013 Carl Pulley <c.j.pulley@hud.ac.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details. 
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 

import analysis
import stp
import vc
from bap.commands.codegen import *

#---------------------------------------------------------------
# Graph Manipulation Commands
#---------------------------------------------------------------

def add_assert(expr="true", bb="BB_Exit", bbN=0):
  "Add the assertion <expr> to line <bbN> of the basic block labelled with <bb>"
  return [["iltrans", "-assert", expr, str(bb), str(bbN)]]

def add_assume(expr="true", bb="BB_Exit", bbN=0):
  "Add the assumption <expr> to line <bbN> of the basic block labelled with <bb>"
  return [["iltrans", "-assume", expr, str(bb), str(bbN)]]

def rm_cycles():
  "Remove cycles/loops within the control-flow graph (CFG)"
  return [["iltrans", "-rm-cycles"]]

def unroll(num, alg="steensgard"):
  """
  Unroll loops <num> times.

  Loops are identified using the following algorithms:
    steensgard - Steensgard's loop forest algorithm: Steensgaard, B. (1993). Sequentializing 
                 Program Dependence Graphs for Irreducible Programs (No. MSR-TR-93-14)
    sa         - structural analysis algorithm based on Advanced Compiler Design & Implementation 
                 by Steven S Muchnick.
  """
  return [["iltrans", "-unroll", str(num), alg]]

def slice_cfg(srcbb=0, srcn=0, trgbb=0, trgn=0):
  """
  Calculate a slice in the control-flow graph (CFG) from a source instruction to a 
  target instruction.

  Slice will contain all instructions that the target instruction depends upon, upto 
  and including the source instruction.

  <srcbb>: source basic block number
  <srcn> : source instruction at which to start slice
  <trgbb>: target basic block
  <trgn> : target instruction at which to end slice
  """
  return [["iltrans", "-ast-chop", str(srcbb), str(srcn), str(trgbb), str(trgn)]]

def rm_indirect_cfg():
  "Remove BB_Indirect within the control-flow graph (CFG)"
  return [["iltrans", "-rm-indirect-ast"]]

def prune_cfg():
  "Prune unreachable nodes from an AST CFG"
  return [["iltrans", "-prune-cfg"]]

def coalesce_cfg():
  "Coalesce adjacent basic blocks within the control-flow graph (CFG)"
  return [["iltrans", "-coalesce-ast"]]

def slice_ssa(srcbb=0, srcn=0, trgbb=0, trgn=0):
  """
  Calculate a slice in the static single assignment (SSA) graph from a source instruction to a 
  target instruction.

  Slice will contain all instructions that the target instruction depends upon, upto 
  and including the source instruction.

  <srcbb>: source basic block number
  <srcn> : source instruction at which to start slice
  <trgbb>: target basic block
  <trgn> : target instruction at which to end slice
  """
  return [["iltrans", "-ssa-chop", str(srcbb), str(srcn), str(trgbb), str(trgn)]]

def rm_indirect_ssa():
  "Remove BB_Indirect with the static single assignment (SSA) graph"
  return [["iltrans", "-rm-indirect-ssa"]]

def prune_ssa():
  "Prune unreachable nodes from a SSA CFG"
  return [["iltrans", "-prune-ssa"]]

def coalesce_ssa():
  "Coalesce adjacent basic blocks within the static single assignment (SSA) graph"
  return [["iltrans", "-coalesce-ssa"]]

#---------------------------------------------------------------
# Graph Conversion Commands
#---------------------------------------------------------------

def to_ast(special_error=True):
  "Convert to abstract syntax tree (AST)"
  return [["iltrans", "-to-ast", str(special_error).lower()]]

def to_cfg(special_error=True):
  "Convert to a control-flow graph (CFG)"
  return [["iltrans", "-to-cfg", str(special_error).lower()]]

def to_ssa(special_error=True):
  "Convert to static single assignment (SSA)"
  return [["iltrans", "-to-ssa", str(special_error).lower()]]

def to_single_stmt_ssa():
  "Create new graph where every node has at most 1 static single assignment (SSA)"
  return [["iltrans", "-single-stmt-ssa"]]

def to_c(filename=None):
  """
  Convert to C  (code must be loop-free!)

  <filename> if specified, output is saved to <filename>
  """
  if filename:
    return [["iltrans", "-to-c", filename]]
  else:
    return [["iltrans", "-to-c"]]

#---------------------------------------------------------------
# Graph Viewing Commands
#---------------------------------------------------------------

def pp_ast(filename=None, varnums=False):
  """
  Print the abstract syntax tree (AST) to stdout

  <filename>  if specified, output is saved to <filename>
  <varnums>   if True, add in (hash) numbers when printing variables
  """
  config = [["iltrans", "-pp-varnums", str(varnums).lower()]]
  if filename == None:
    return config + [["iltrans", "-pp-ast"]]
  else:
    return config + [["iltrans", "-pp-ast", filename]]

def pp_cfg(filename=None, convert=None, stmts=True, bbids=True, asms=True, varnums=False, linenums=True, struct=True):
  """
  Print the control flow graph (CFG) in dot format

  <filename> : if specified, output is saved to <filename>
  <convert>  : if specified, translate SSA graph to control dependence graph (CDG), program dependence graph (PDG) or data dependence graph (DDG) form
  <stmts>    : if True, include printing of BAP IL statements
  <bbids>    : if True, include printing of basic block IDs
  <asms>     : if True, include printing of @asm attributes (i.e. assembler statements)
  <varnums>  : if True, add in (hash) numbers when printing variables
  <linenums> : if True, print line numbers for each block statement
  <struct>   : if True, encapsulate basic blocks in structural regions
  """
  config = [["iltrans", "-pp-varnums", str(varnums).lower()]]
  cmd = ["iltrans", "-pp-cfg", (convert or "none").lower(), str(stmts).lower(), str(bbids).lower(), str(asms).lower(), str(linenums).lower(), str(struct).lower()]
  if filename == None:
    return config + [cmd]
  else:
    return config + [cmd + [filename]]

def pp_ssa(filename=None, convert=None, stmts=True, bbids=True, asms=True, varnums=False, linenums=True, struct=False):
  """
  Print the static single assignment (SSA) graph in dot format

  <filename> : if specified, output is saved to <filename>
  <convert>  : if specified, translate SSA graph to control dependence graph (CDG), program dependence graph (PDG) or data dependence graph (DDG) form
  <stmts>    : if True, include printing of BAP IL statements
  <bbids>    : if True, include printing of basic block IDs
  <asms>     : if True, include printing of @asm attributes (i.e. assembler statements)
  <varnums>  : if True, add in (hash) numbers when printing variables
  <linenums> : if True, print line numbers for each block statement
  <struct>   : if True, encapsulate basic blocks in structural regions (NOT IMPLEMENTED)
  """
  config = [["iltrans", "-pp-varnums", str(varnums).lower()]]
  if struct:
    raise Exception("TODO: SSA structural analysis is not currently implemented")
  cmd = ["iltrans", "-pp-ssa", (convert or "none").lower(), str(stmts).lower(), str(bbids).lower(), str(asms).lower(), str(linenums).lower(), str(struct).lower()]
  if filename == None:
    return config + [cmd]
  else:
    return config + [cmd + [filename]]

#---------------------------------------------------------------
# Command Controlling if BAP Values are Returned from Call
#---------------------------------------------------------------

def return_term():
  """
  Return BAP term, solver model (if defined) and WP (if defined). Only those values
  related to the last occurrence of this command (in a command list) are returned.

  BAP term will be a Python abstract syntax tree representing:
    -AST parse tree, if term is abstract syntax tree (AST)
    -CFG graph term, if term is control flow graph (CFG)
    -SSA graph term, if term is static single assignment (SSA).

  Solver model will be a dictionary representing an invalidating interpretation 
  mapping variables to integers.

  WP will be a dictionary containing:
    -an abstract syntax tree of the calculated pre-condition
    -a list of universally quantified variables.
  """
  return [["control", "-return"]]
